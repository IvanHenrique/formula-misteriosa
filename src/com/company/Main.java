package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner ler = new Scanner(System.in);
        System.out.println("Teste de lógica! \n Formula Misteriosa");

        System.out.println("Entrada:");

        long num = ler.nextInt();
        System.out.println("Entrada: " + num);

        System.out.println("Saida: " + formulaSecreta(num));
    }

    private static long formulaSecreta(long entrada) {
        String saida = "";
        String aux = String.valueOf(entrada);
        String charAnterior = aux.substring(0, 1);
        int ocorrencias = 1;

        for (int i = 1; i < aux.length(); i++)
        {
            String charAtual = aux.substring(i, i + 1);
            if (!charAtual.equals(charAnterior))
            {
                saida += String.valueOf(ocorrencias) + charAnterior;
                ocorrencias = 0;
            }
            charAnterior = charAtual;
            ocorrencias++;
        }
        //Para pegar o ultimo char
        saida += String.valueOf(ocorrencias) + charAnterior;

        return Long.parseLong(saida);
    }

}
